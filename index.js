// TODO
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = process.env.PORT ?? 56201;

app.use(bodyParser.text());
//task1
app.post('/square', bodyParser.text(), (req, res) => {
	const num = +req.body;
	if (!isNaN(num)) {
	  const result = num*num;
	  res.send({
	    number: num,
	    square: result,
	  });
	}
	else {
		res.status(400).send("Invalid value");
	}
});

//task2
app.post('/reverse', bodyParser.text(), (req, res) => {
	const input = req.body ?? "";
	let result = "";
	for (let i = input.length - 1; i >= 0; i--) {
    	result += input[i];
  	}
	res.send(result);
});

//task3
app.get('/date/:year/:month/:day', (req, res) => {
	const y = +req.params.year;
	const m = +req.params.month;
	const d = +req.params.day;
	const date = new Date(y,m-1,d);
	if (date.getFullYear() !== y ||
		date.getMonth() !== m-1 ||
		date.getDate() !== d) {
		res.status(400).send("Invalid date");
		return;
	}
	const today = new Date();
	res.send({
		weekDay: date.toLocaleString('en-us',{weekday: 'long'}),
		isLeapYear: ((y % 4 === 0 && y % 100 !== 0) || y % 400 === 0),
		difference: Math.abs(Math.floor((today - date) / (1000 * 60 * 60 * 24)))
	})
});

app.listen(port, () => {
  console.log(`Course-task-01 listening on port ${port}`);
});